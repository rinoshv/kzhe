var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-minify-css'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant'),
    rimraf = require('rimraf'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

var path = {
  build: {
    html: 'www/',
    js: 'www/js/',
    css: 'www/css/',
    img: 'www/assets/images/',
    fonts: 'www/assets/webfonts/'
  },
  src: {
    html: 'src/app/*.html',
    js: 'src/app/js/*.js',
    style: 'src/app/sass/main.scss',
    img: 'src/assets/images/**/*.*',
    fonts: 'src/assets/webfonts/**/*.*'
  },
  watch: {
    html: 'src/app/**/*.html',
    js: 'src/app/js/**/*.js',
    style: 'src/app/sass/**/*.scss',
    img: 'src/assets/images/**/*.*',
    fonts: 'src/assets/webfonts/**/*.*'
  },
  clean: './www'
};

var config = {
  server: {
    baseDir: './www'
  },
  tunnel: true,
  host: 'localhost',
  port: 4000
};

gulp.task('webserver', function () {
  browserSync(config);
});

gulp.task('clean', function (cb) {
  rimraf(path.clean, cb);
});

gulp.task('html:build', function () {
  gulp.src(path.src.html)
    .pipe(rigger())
    .pipe(gulp.dest(path.build.html))
    .pipe(reload({stream: true}));
});

gulp.task('js:build', function () {
  gulp.src(path.src.js)
    .pipe(rigger())
    .pipe(sourcemaps.init())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({stream: true}));
});

gulp.task('style:build', function () {
  gulp.src(path.src.style)
    .pipe(sass({
      errLogToConsole: false
    }))
    .pipe(prefixer())
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({stream: true}));
});

gulp.task('image:build', function () {
  gulp.src(path.src.img)
    .pipe(gulp.dest(path.build.img))
    .pipe(reload({stream: true}));
});

gulp.task('fonts:build', function() {
  gulp.src(path.src.fonts)
    .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
  'html:build',
  'js:build',
  'style:build',
  'fonts:build',
  'image:build'
]);

gulp.task('watch', function(){
  watch([path.watch.html], function(event, cb) {
    gulp.start('html:build');
  });
  watch([path.watch.style], function(event, cb) {
    gulp.start('style:build');
  });
  watch([path.watch.js], function(event, cb) {
    gulp.start('js:build');
  });
});

gulp.task('default', ['build', 'webserver', 'watch']);
