$(document).ready(function() {
  $('select').niceSelect();
});

$(document).ready(function(){
  $('#searchbar-icon').click(function(){
    $('#searchbar-input').animate({width: 'toggle'});
    $("#searchbar-icon").toggle();
    $("#searchbar-cross").toggle(500);
  });

  $('#searchbar-cross').click(function(){
    $('#searchbar-input').animate({width: 'toggle'});
    $("#searchbar-cross").toggle();
    $("#searchbar-icon").toggle(500);
  });
});

jQuery(document).ready(function(){
	$('.menu-item').addClass('menu-trigger');
	$('.menu-trigger').click(function(){
		$('#menu-trigger').toggleClass('clicked');
		$('.container').toggleClass('push');
		$('.menu-type').toggleClass('open');
		$('body').toggleClass('body');
	});
});

// $(function() {
//   $('.intro').addClass('go');
// })

// //header
// $(document).ready(function() {
// 	"use strict";
// 	var myNav = {
// 		init: function() {
// 			this.cacheDOM();
// 			this.browserWidth();
// 			this.bindEvents();
// 		},
// 		cacheDOM: function() {
// 			this.navBars = $(".navBars");
// 			this.xBxHack = $("#xBxHack");
// 			this.navMenu = $("#menu");
// 		},
// 		browserWidth: function() {
// 			$(window).resize(this.bindEvents.bind(this));
// 		},
// 		bindEvents: function() {
// 			var width = window.innerWidth;

// 			if (width < 992) {
// 				this.navBars.click(this.animate.bind(this));
// 				this.navMenu.hide();
// 				this.xBxHack[0].checked = false;
// 			} else {
// 				this.resetNav();
// 			}
// 		},
// 		animate: function(e) {
// 			var checkbox = this.xBxHack[0];
// 			!checkbox.checked ?
// 				this.navMenu.slideDown() :
// 				this.navMenu.slideUp();

// 		},
// 		resetNav: function() {
// 			this.navMenu.show();
// 		}
// 	};
// 	myNav.init();
// });

// //long-text
// // var size = 75,
// //     newsContent= $('.long-text'),
// //     newsText = newsContent.text();

// // if (newsText.length > size){
// // 	newsContent.text(newsText.slice(0, size) + ' ...');
// // }
// $(function(){
//     $("#datepicker").datepicker();
// });

// //comments
// /**
// *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
// *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
// /*
// var disqus_config = function () {
// this.page.url = PAGE_URL;  // Replace PAGE_URL with your page's canonical URL variable
// this.page.identifier = PAGE_IDENTIFIER; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
// };
// */
// (function() { // DON'T EDIT BELOW THIS LINE
// var d = document, s = d.createElement('script');
// s.src = 'https://kzhe.disqus.com/embed.js';
// s.setAttribute('data-timestamp', +new Date());
// (d.head || d.body).appendChild(s);
// })();
